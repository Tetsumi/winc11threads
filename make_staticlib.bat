rm *.o
gcc -I.\source\ -DNDEBUG -D_WIN32_WINNT=0x0600 -std=c11 -O2 -c source\call_once.c
gcc -I.\source\ -DNDEBUG -D_WIN32_WINNT=0x0600 -std=c11 -O2 -c source\cnd_*.c
gcc -I.\source\ -DNDEBUG -D_WIN32_WINNT=0x0600 -std=c11 -O2 -c source\mtx_*.c
gcc -I.\source\ -DNDEBUG -D_WIN32_WINNT=0x0600 -std=c11 -O2 -c source\thrd_*.c
gcc -I.\source\ -DNDEBUG -D_WIN32_WINNT=0x0600 -std=c11 -O2 -c source\tss_*.c
ar rcs lib\libwinc11threads.a *.o