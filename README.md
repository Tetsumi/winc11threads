winc11threads
===================

An implementation of threads.h from the C11's Standard for Microsoft Windows. Windows Vista 64 bits and later are supported. 


![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)

Compilation:
------------------
Tested with Mingw-w64 GCC 4.9.1.

To compile this code, the minimum value for [ _WIN32_WINNT](https://msdn.microsoft.com/en-us/library/windows/desktop/aa383745%28v=vs.85%29.aspx) must be _WIN32_WINNT_VISTA.

You can either compile winc11threads directly in your code with the file winc11threads.c

EG: `gcc -DNDEBUG -D_WIN32_WINNT=0x0600 -std=c11 winc11threads.c yourcode.c`

or to a library by executing the file make_staticlib.bat.

Documentation:
------------------
http://en.cppreference.com/w/c/thread


