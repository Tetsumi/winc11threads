/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>

#include "test.h"


#define NUM_OF_MUTEX_TYPES 4

unsigned int g_num_threads = 1000;

struct
{
	const int type;
	const char const *caption;
} mutex_types[NUM_OF_MUTEX_TYPES] = {
	{.type = mtx_plain,                 .caption = "Plain"},
	{.type = mtx_plain | mtx_recursive, .caption = "Plain recursive"},
	{.type = mtx_timed,                 .caption = "Timed"},
	{.type = mtx_timed | mtx_recursive, .caption = "Timed recursive"}
};

typedef Error (*TestProc)(size_t num_threads, int mtx_type);

typedef struct
{
	TestProc proc;
	const char const *caption;
} Test;

const Test tests[] = {
	{.proc = test_mutexsharedcounter, .caption = "Mutex shared counter"},
	{.proc = test_mutexrecursion,     .caption = "Mutex recursion"},
	{.proc = test_mutextimed,         .caption = "Mutex timed lock"},
	{.proc = test_callonce,           .caption = "Call once"},
	{.proc = test_cond,               .caption = "Condition variable"},
	{.proc = test_condtimed,          .caption = "Condition variable timed"},
	{.proc = NULL,                    .caption = "End mark"}
};

const char *error_captions[] = {
	[E_SUCCESS]	     = "Success",
	[E_UNKNOWN]	     = "Error: Unknown",
	[E_TIMEDOUT]	     = "Error: Timed out",
	[E_THREAD_CREATE]    = "Error: Thread creation failed",
	[E_THREAD_JOIN]	     = "Error: Thread join failed",
	[E_WRONG_RESULT]     = "Error: Wrong result",
	[E_BUSY]	     = "Error: Busy",
	[E_MUTEX_INIT]	     = "Error: Mutex initialization failed",
	[E_MUTEX_LOCK]	     = "Error: Mutex locking failed",
	[E_MUTEX_UNLOCK]     = "Error: Mutex unlocking failed",
	[E_MUTEX_TRY_LOCK]   = "Error: Mutex try locking failed",
	[E_RECURSION]	     = "Error: Mutex recursion failed",
	[E_MUTEX_TIMED_LOCK] = "Error: Mutex timed locking failed",
	[E_COND_INIT]        = "Error: Condition Variable initialization failed",
	[E_COND_WAIT]        = "Error: Waiting for condition variable failed"
	
};

static void do_test (const Test * const test)
{
	printf("Test: %s\n", test->caption);
	
	for (int i = 0; i < NUM_OF_MUTEX_TYPES; ++i) {
		printf("  with %-30s", mutex_types[i].caption);
		
		Error ret = test->proc(g_num_threads, mutex_types[i].type);

		puts(error_captions[ret]);
	}

	puts("");
}

int main (int argc, char *argv[])
{
	printf("winc11threads Tests\n\n");

	if (argc > 1) {
		int arg = atoi(argv[1]);
		
		if (arg > 0)
			g_num_threads = arg;
		else {
			puts("Number of thread must be superior to zero.");
			return EXIT_SUCCESS;
		}
	}

	int test_counter = 0;

	while (tests[test_counter].proc) {
		do_test(&tests[test_counter]);
		
		++test_counter;
	}
	
}
