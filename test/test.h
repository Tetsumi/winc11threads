#pragma once
#include <threads.h>

typedef enum
{
	E_SUCCESS,
	E_UNKNOWN,
	E_TIMEDOUT,
	E_THREAD_CREATE,
	E_THREAD_JOIN,
	E_WRONG_RESULT,
	E_BUSY,
	E_MUTEX_INIT,
	E_MUTEX_LOCK,
	E_MUTEX_UNLOCK,
	E_MUTEX_TRY_LOCK,
	E_RECURSION,
	E_MUTEX_TIMED_LOCK,
	E_COND_INIT,
	E_COND_WAIT,
	
} Error;


Error test_mutexsharedcounter (size_t num_threads, int mtx_type);
Error test_mutexrecursion (size_t num_threads, int mtx_type);
Error test_mutextimed (size_t num_threads, int mtx_type);
Error test_callonce (size_t num_threads, int mtx_type);
Error test_cond (size_t num_threads, int mtx_type);
Error test_condtimed (size_t num_threads, int mtx_type);
