/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "test.h"

static mtx_t g_mutex;
static cnd_t g_cond;

static int thread_func (void *arg)
{
	if (thrd_success != mtx_lock(&g_mutex))
		return E_MUTEX_LOCK;

	struct _timeb curr;
	int ret = -1;

	_ftime(&curr);

	const struct timespec ts = {
		.tv_sec = curr.time, 
		.tv_nsec = 1000000 * (curr.millitm + 10) 
	};

	// Looping because Windows produces many spurious wakeups
	// when multiple threads do a timed wait on a condition
	// variable.
	while (ret != E_SUCCESS) {
		switch (cnd_timedwait(&g_cond, &g_mutex, &ts)) {
		case thrd_error:
			ret = E_COND_WAIT;
			break;
		case thrd_timedout:
			ret = E_SUCCESS;
			break;
		default:
			break;
		}
	}

	if (thrd_success != mtx_unlock(&g_mutex))
		return E_MUTEX_LOCK;
	
	return ret;
}

Error test_condtimed (size_t num_threads, int mtx_type)
{
	thrd_t threads[num_threads];


	if (thrd_success != mtx_init(&g_mutex, mtx_type))
		return E_MUTEX_INIT;

	if (thrd_success != cnd_init(&g_cond))
		return E_COND_INIT;
	
	for (int i = 0; i < num_threads; ++i) {
		if (thrd_success != thrd_create(&threads[i], thread_func, (void*)(unsigned long long)i))
			return E_THREAD_CREATE;
	}


	for (int i = 0; i < num_threads; ++i) {
		int ret = 0;

		if (thrd_success != thrd_join(threads[i], &ret))
			return E_THREAD_JOIN;

		if (E_SUCCESS != ret)
			return ret;
	}

	mtx_destroy(&g_mutex);
	cnd_destroy(&g_cond);
	
	return E_SUCCESS;
}
