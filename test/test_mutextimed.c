/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "test.h"

static mtx_t g_mutex;

static int thread_func (void *arg)
{
	struct _timeb curr;
	
	_ftime(&curr);

	const struct timespec ts = {
		.tv_sec = curr.time, 
		.tv_nsec = 1000000 * (curr.millitm + 10) 
	};
	
	if (thrd_timedout != mtx_timedlock(&g_mutex, &ts))
		return E_MUTEX_TIMED_LOCK;
	
	return E_SUCCESS;
}

Error test_mutextimed (size_t num_threads, int mtx_type)
{

	if (!(mtx_type & mtx_timed))
		return E_SUCCESS;
	
	thrd_t threads[num_threads];

	if (thrd_success != mtx_init(&g_mutex, mtx_type))
		return E_MUTEX_INIT;
	
	mtx_lock(&g_mutex);
	
	for (int i = 0; i < num_threads; ++i) {
		if (thrd_success != thrd_create(&threads[i], thread_func, NULL))
			return E_THREAD_CREATE;
	}

	for (int i = 0; i < num_threads; ++i) {
		int ret = 0;

		if (thrd_success != thrd_join(threads[i], &ret))
			return E_THREAD_JOIN;

		if (E_SUCCESS != ret)
			return ret;
	}

	mtx_unlock(&g_mutex);
	mtx_destroy(&g_mutex);
			
	return E_SUCCESS;
}
