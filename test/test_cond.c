/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "test.h"

static mtx_t g_mutex;
static cnd_t g_cond;
static unsigned int g_counter;

static int thread_func (void *arg)
{
	if (thrd_success != mtx_lock(&g_mutex))
		return E_MUTEX_LOCK;

	++g_counter;

	if (thrd_success != cnd_wait(&g_cond, &g_mutex))
		return E_COND_WAIT;
	
	if (thrd_success != mtx_unlock(&g_mutex))
		return E_MUTEX_UNLOCK;
	
	return E_SUCCESS;
}

Error test_cond (size_t num_threads, int mtx_type)
{
	thrd_t threads[num_threads];
	g_counter = 0;

	if (thrd_success != mtx_init(&g_mutex, mtx_type))
		return E_MUTEX_INIT;

	if (thrd_success != cnd_init(&g_cond))
		return E_COND_INIT;
	
	for (int i = 0; i < num_threads; ++i) {
		if (thrd_success != thrd_create(&threads[i], thread_func, NULL))
			return E_THREAD_CREATE;
	}

	while (g_counter < num_threads)
		thrd_sleep(&(struct timespec){
				.tv_sec = 0,
				.tv_nsec = 1000000 * 120
			   },
		           NULL);

	cnd_broadcast(&g_cond);

	for (int i = 0; i < num_threads; ++i) {
		int ret = 0;

		if (thrd_success != thrd_join(threads[i], &ret))
			return E_THREAD_JOIN;

		if (E_SUCCESS != ret)
			return ret;
	}

	mtx_destroy(&g_mutex);
	cnd_destroy(&g_cond);
	
	return E_SUCCESS;
}
