/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>

int cnd_init (cnd_t *cond)
{
	assert(cond);

	const HANDLE one = CreateEvent(NULL, FALSE, FALSE, NULL);
	const HANDLE all = CreateEvent(NULL, TRUE, FALSE, NULL);

	if (!one || !all) {
		CloseHandle(one);
		CloseHandle(all);
		
		return thrd_error;
	}
	
	InitializeConditionVariable(&cond->cv);
	
	cond->timed_counter = 0;
	cond->event_one = one;
	cond->event_all = all;
	
	return thrd_success;
}
