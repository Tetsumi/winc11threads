/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>

int mtx_lock (mtx_t *mtx)
{
	assert(mtx);

	DWORD ret = 0u;

	switch (mtx->type) {
	case mtx_plain:
		AcquireSRWLockExclusive(&mtx->srwl);
		break;
		
	case mtx_plain | mtx_recursive:	
		EnterCriticalSection(&mtx->cs);
		break;
		
	case mtx_timed:	
	case mtx_timed | mtx_recursive:
		ret = WaitForSingleObject(mtx->mutsem, INFINITE);

		if (WAIT_FAILED == ret || WAIT_ABANDONED == ret)
			return thrd_error;
		break;
	}

	return thrd_success;
}
