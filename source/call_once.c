/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>

BOOL CALLBACK wrapper (PINIT_ONCE InitOnce,
		       PVOID Parameter,
		       PVOID *lpContext)
{
	((void (*)(void))Parameter)();

	return TRUE;
}

void call_once (once_flag *flag, void (*func)(void))
{
	assert(flag);
	assert(func);

	InitOnceExecuteOnce(flag, wrapper, func, NULL);	
}
