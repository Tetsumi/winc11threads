/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>

#if _WIN32_WINNT >= _WIN32_WINNT_WIN8
#define __get_filetime GetSystemTimePreciseAsFileTime
#else
#define __get_filetime GetSystemTimeAsFileTime
#endif

int thrd_sleep (const struct timespec *duration, struct timespec *remaining)
{
	assert(duration);

	typedef union {
		FILETIME ft;
		ULARGE_INTEGER uli;
	} timequad_t;

	DWORD toWait = (duration->tv_sec * 1000) + (duration->tv_nsec / 1000000);

	timequad_t bef;
	
	if (remaining)
		__get_filetime(&bef.ft);
	
	DWORD ret = SleepEx(toWait, TRUE);

	if (ret) {
		if (remaining) {
			timequad_t af;
			
			__get_filetime(&af.ft);

			af.uli.QuadPart -= bef.uli.QuadPart;

			*remaining = (struct timespec) {
				.tv_sec = af.uli.QuadPart / 10000000,
				.tv_nsec = (af.uli.QuadPart % 10000000) * 100
			};
		}
		
		return -1;
	}
	
	return 0;
}
