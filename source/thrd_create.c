/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>

int thrd_create (thrd_t *thr, thrd_start_t func, void *arg)
{
	assert(thr);
	assert(func);
	
	HANDLE hThread = CreateThread(NULL,
				      0,
				      (LPTHREAD_START_ROUTINE)func,
				      arg,
				      0,
				      NULL);

	if (!hThread) {
		return GetLastError() == ERROR_NOT_ENOUGH_MEMORY ?
			thrd_nomem : thrd_error;
	}

	*thr = hThread;

	return thrd_success;
}
