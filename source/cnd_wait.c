/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>

int cnd_wait (cnd_t* cond, mtx_t* mutex)
{
	assert(cond);
	assert(mutex);

	switch (mutex->type) {
	case mtx_plain:
		if (!SleepConditionVariableSRW(&cond->cv,
					       &mutex->srwl,
					       INFINITE,
					       0UL))
			return thrd_error;
		break;
		
	case mtx_plain | mtx_recursive:
		if (!SleepConditionVariableCS(&cond->cv,
					      &mutex->cs,
					      INFINITE))
			return thrd_error;
		break;
		
	case mtx_timed:		
	case mtx_timed | mtx_recursive:
		++(cond->timed_counter);
		mtx_unlock(mutex);
		
		DWORD rt = WaitForMultipleObjects(2,
						  (HANDLE []){
							  cond->event_all,
							  cond->event_one
						  },
						  FALSE,
						  INFINITE);
		mtx_lock(mutex);		
		size_t index = --(cond->timed_counter);
	
		if (WAIT_FAILED == rt 
		    || (WAIT_OBJECT_0 == rt
			&& 0 == index
			&& !ResetEvent(cond->event_all))) {
			mtx_unlock(mutex);
			return thrd_error;
		}
						
		break;	
	}
	
	
	return thrd_success;
}
