/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>

int tss_create (tss_t *key, tss_dtor_t dtor)
{
	assert(key);

	DWORD fls = FlsAlloc((PFLS_CALLBACK_FUNCTION)dtor);

	if (FLS_OUT_OF_INDEXES == fls)
		return thrd_error;

	*key = fls;

	return thrd_success;
}
