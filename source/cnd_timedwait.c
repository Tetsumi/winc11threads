/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>
int cnd_timedwait (cnd_t* restrict cond,
		   mtx_t* restrict mutex,
		   const struct timespec* restrict time_point)
{
	assert(cond);
	assert(mutex);
	assert(time_point);

	struct _timeb curr;
	
	_ftime(&curr);
		
	const DWORD toWait = ((time_point->tv_sec - curr.time) * 1000)
		           + ((time_point->tv_nsec / 1000000) - curr.millitm);

	if ((int)toWait < 0)
		return thrd_timedout;

	unsigned long long int tce = (time_point->tv_sec * 1000) + (time_point->tv_nsec / 1000000);

	
	switch (mutex->type) {
	case mtx_plain:
		if (!SleepConditionVariableSRW(&cond->cv,
					       &mutex->srwl,
					       toWait,
					       0UL)) {      
			return (GetLastError() == ERROR_TIMEOUT) ?
				thrd_timedout : thrd_error;
		}
		break;
		
	case mtx_plain | mtx_recursive:	
		if (!SleepConditionVariableCS(&cond->cv,
					      &mutex->cs,
					      toWait)) {
			return (GetLastError() == ERROR_TIMEOUT) ?
				thrd_timedout : thrd_error;
		}
		break;
		
	case mtx_timed:		
	case mtx_timed | mtx_recursive:
		++(cond->timed_counter);
		mtx_unlock(mutex);
		
		DWORD rt = WaitForMultipleObjects(2,
						  (HANDLE []){
							  cond->event_all,
							  cond->event_one
						  },
						  FALSE,
						  toWait);
		mtx_lock(mutex);		
		size_t index = --(cond->timed_counter);
	
		if (WAIT_FAILED == rt 
		    || (WAIT_OBJECT_0 == rt
			&& 0 == index
			&& !ResetEvent(cond->event_all))) {
			mtx_unlock(mutex);
			return thrd_error;
		} else if (WAIT_TIMEOUT == rt)
			return thrd_timedout;
						
		break;
	}

	return thrd_success;
}
