/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "call_once.c"
#include "cnd_broadcast.c"
#include "cnd_destroy.c"
#include "cnd_init.c"
#include "cnd_signal.c"
#include "cnd_timedwait.c"
#include "cnd_wait.c"
#include "mtx_destroy.c"
#include "mtx_init.c"
#include "mtx_lock.c"
#include "mtx_timedlock.c"
#include "mtx_trylock.c"
#include "mtx_unlock.c"
#include "thrd_create.c"
#include "thrd_current.c"
#include "thrd_detach.c"
#include "thrd_equal.c"
#include "thrd_exit.c"
#include "thrd_join.c"
#include "thrd_sleep.c"
#include "thrd_yield.c"
#include "tss_create.c"
#include "tss_get.c"
#include "tss_set.c"
