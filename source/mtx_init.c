/*
  Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <threads.h>
#include <assert.h>

int mtx_init (mtx_t *mtx, int type)
{
	assert(mtx);

	mtx->type = type;

	HANDLE handle = NULL;

	switch (type) {
	case mtx_plain:
		InitializeSRWLock(&mtx->srwl);
		break;
		
	case mtx_plain | mtx_recursive:	
		InitializeCriticalSection(&mtx->cs);
		break;
		
	case mtx_timed:
		handle = CreateSemaphore(NULL, 1, 1, NULL);
		
		if (!handle)
			return GetLastError() == ERROR_NOT_ENOUGH_MEMORY ?
				thrd_nomem : thrd_error;
		
		mtx->mutsem = handle;
		break;
		
	case mtx_timed | mtx_recursive:
		handle = CreateMutex(NULL, 0, NULL);
		
		if (!handle)
			return GetLastError() == ERROR_NOT_ENOUGH_MEMORY ?
				thrd_nomem : thrd_error;
		
		mtx->mutsem = handle;
		break;
	}

	return thrd_success;
}
